var map
var player
var sheep : Node2D

var quick_time : float = .5
var low_time = 4 

var velocity : float
var direction : Vector2

var run_velocity : float
var wait_velocity : float
var fear_the_player : bool
var is_born: bool

func _init(_map, _sheep:Node2D):
	map = _map
	sheep = _sheep
	run_velocity  = 200
	wait_velocity = 0
	velocity = wait_velocity
	fear_the_player = false
	var fear_zone : Area2D = sheep.get_node("FearZone") as Area2D
	fear_zone.area_entered.connect(_on_area_entered)
	fear_zone.area_exited.connect(_on_area_exited)
	is_born = false

func set_is_born():
	is_born = true
	
func ef_post_update(_state_machine, delta):
	direction = -(player.position - sheep.position)
	direction = direction/direction.length()
	sheep.position += direction * velocity * delta
	sheep.rotation = atan2(direction.x, -direction.y)
	if not fear_the_player:
		sheep.rotation += PI

func _on_area_entered(area):
	if area == player:
		fear_the_player = true

func _on_area_exited(area):
	if area == player:
		fear_the_player = false

## _sm is the machine state
## _state_id is the state id incident of the crossed transitions.
## _trans_ids is the array of all transitions ids that had been crossed.
## _nb_micro_update is the current number of micro_update minus 1.
## (To obtain the update counter, use the machine state _sm)
func ef_start(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	player = map.get_node("Player")
	return stop_update

## _sm is the machine state
## _state_id is the state id incident of the crossed transitions.
## _trans_ids is the array of all transitions ids that had been crossed.
## _nb_micro_update is the current number of micro_update minus 1.
## (To obtain the update counter, use the machine state _sm)
func ef_start_waiting(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	velocity = wait_velocity
	return stop_update

## _sm is the machine state
## _state_id is the state id incident of the crossed transitions.
## _trans_ids is the array of all transitions ids that had been crossed.
## _nb_micro_update is the current number of micro_update minus 1.
## (To obtain the update counter, use the machine state _sm)
func ef_start_fleeing(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	velocity = run_velocity
	return stop_update


## _sm is the machine state
## _state_id is the state id of the update function.
## _delay is the time in seconds since the last state machine update.
func ef_do_nothing(_sm: SMStateMachine, _state_id:int, _delay:float) -> bool:
	var stop_update = false
	return stop_update

## _sm is the machine state
## _state_id is the state id of the update function.
## _delay is the time in seconds since the last state machine update.
func ef_flee(_sm: SMStateMachine, _state_id:int, _delay:float) -> bool:
	var stop_update = false
	return stop_update

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_always_true(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return true

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_always_false(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return false

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_is_fearing(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return fear_the_player

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_is_ok(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return not fear_the_player

## _sm is the machine state
## _trans_id is the transtion id of the crossed transition.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_gives_birth_to_sheep(_sm: SMStateMachine, _trans_id:int, _nb_micro_update:int) -> bool:
	var stop_update = false
	sheep.visible = true
	return stop_update

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_do_we_give_birth_to_sheep(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return is_born

extends RefCounted

var traduction_files = [
	preload("res://texts/01_dialogue_en.po"),
	preload("res://texts/01_dialogue_fr.po")
]

var dialog_file = "res://texts/01_dialogue.json"
var dialog : LinearDialog = null
var dialog_variables = {}
var total_sheep = 2
var limit_time = 60
var map_ressource = preload("res://map.tscn")
var map
var main_scene
var player_ressource = preload("res://player.tscn")
var player
var sheep_ressource = preload("res://sheep.tscn")
var sheeps : Array
var maximal_sheep_number = 3
var rng : RandomNumberGenerator
var inserted_sheep = 0

var captured_sheep :  int
func _on_enclodure_entered(area):
	if area.name.contains("Sheep"):
		captured_sheep += 1
		
func _on_enclodure_exited(area):
	if area.name.contains("Sheep"):
		captured_sheep -= 1
	
func _init(_main_scene):
	main_scene = _main_scene
	for trad in traduction_files:
		TranslationServer.add_translation( trad )
	
	dialog = LinearDialog.new(dialog_file)
	dialog_variables = {
		"total_sheep" : total_sheep,
		"limit_time" : str(limit_time)
	}
	rng = RandomNumberGenerator.new()
	player_is_at_home = true

var player_is_at_home : bool
func _on_at_home_entered(area):
	if area == player:
		player_is_at_home = true
		
func _on_at_home_exited(area):
	if area == player:
		player_is_at_home = false
	
func dispose():
	map.queue_free()
	
## _sm is the machine state
## _state_id is the state id incident of the crossed transitions.
## _trans_ids is the array of all transitions ids that had been crossed.
## _nb_micro_update is the current number of micro_update minus 1.
## (To obtain the update counter, use the machine state _sm)
func ef_start_example(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	map = map_ressource.instantiate()
	main_scene.get_node("Scenario").add_child(map)
	player = player_ressource.instantiate()
	player.position = map.get_node("StartingPosition").position
	map.add_child(player)
	map.get_node("SheepEnclosure").area_entered.connect(_on_enclodure_entered)
	map.get_node("SheepEnclosure").area_exited.connect(_on_enclodure_exited)
	map.get_node("Home").area_entered.connect(_on_at_home_entered)
	map.get_node("Home").area_exited.connect(_on_at_home_exited)

	for i in range(maximal_sheep_number):
		var sheep = sheep_ressource.instantiate()	
		sheep.init(map)
		sheep.name = StringName( "Sheep" + "_" + str(i) )
		sheep.visible = false
		map.add_child(sheep)
		sheeps.append(sheep)
	inserted_sheep = 0

	return stop_update

## _sm is the machine state
## _state_id is the state id incident of the crossed transitions.
## _trans_ids is the array of all transitions ids that had been crossed.
## _nb_micro_update is the current number of micro_update minus 1.
## (To obtain the update counter, use the machine state _sm)
func ef_start_dialog(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	dialog.start()
	clear_all_text()
	return stop_update

func center(_text):
	return "[p align=\"center\"]" + _text

## _sm is the machine state
## _state_id is the state id incident of the crossed transitions.
## _trans_ids is the array of all transitions ids that had been crossed.
## _nb_micro_update is the current number of micro_update minus 1.
## (To obtain the update counter, use the machine state _sm)
func ef_display_win(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	clear_all_text()
	map.get_node("Display/Text").append_text(center("VICTORY"))
	return stop_update

## _sm is the machine state
## _state_id is the state id incident of the crossed transitions.
## _trans_ids is the array of all transitions ids that had been crossed.
## _nb_micro_update is the current number of micro_update minus 1.
## (To obtain the update counter, use the machine state _sm)
func ef_display_game_over(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	clear_all_text()
	map.get_node("Display/Text").append_text(center("GAME OVER"))
	return stop_update

func clear_all_text():
	map.get_node("Display/Text").clear()
	map.get_node("Field/Text").clear()
	map.get_node("Home/Text").clear()
	map.get_node("SheepEnclosure/Text").clear()
	map.get_node("Player/Text").clear()

## _sm is the machine state
## _state_id is the state id of the update function.
## _delay is the time in seconds since the last state machine update.
func ef_update_dialog(_sm: SMStateMachine, _state_id:int, _delay:float) -> bool:
	var stop_update = false
	
	var line = dialog.next(_delay, false)
	if LinearDialog.line_contains_text(line):
		var texts = LinearDialog.get_texts_line(line)
		clear_all_text()

		if len(texts) != 0:
			for charac in texts:
				if charac == "AF":
					map.get_node("Display/Text").append_text(
						center(texts[charac].translate(dialog_variables))
					)
				elif charac == "CH":
					map.get_node("Field/Text").append_text(
						center(texts[charac].translate(dialog_variables))
					)
				elif charac == "EN":
					map.get_node("SheepEnclosure/Text").append_text(
						center(texts[charac].translate(dialog_variables))
					)
				elif charac == "JO":
					map.get_node("Player/Text").append_text(
						center(texts[charac].translate(dialog_variables))
					)
				elif charac == "MA":
					map.get_node("Home/Text").append_text(
						center(texts[charac].translate(dialog_variables))
					)
				else:
					DBG.print("Unknonw text")
	return stop_update

var time_spawn = 3
var last_spawn = 0


## _sm is the machine state
## _state_id is the state id incident of the crossed transitions.
## _trans_ids is the array of all transitions ids that had been crossed.
## _nb_micro_update is the current number of micro_update minus 1.
## (To obtain the update counter, use the machine state _sm)
func ef_start_spawn(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	last_spawn = 0
	return stop_update


## _sm is the machine state
## _state_id is the state id of the update function.
## _delay is the time in seconds since the last state machine update.
func ef_update_spawn(_sm: SMStateMachine, _state_id:int, _delay:float) -> bool:
	var stop_update = false
	
	map.get_node("Display/Text").clear()
	var text_ = (
		"temps : " + str(int(count_dowm(_sm))) +
		"\nCapture : " + str(captured_sheep) + "/" + str(total_sheep)
	)
	if captured_sheep >= total_sheep :
		text_ += "\nGo to home !"
		
	map.get_node("Display/Text").append_text(center(text_) ) 
		
	var time = _sm.get_time()
	if ( time - last_spawn) < time_spawn:
		return stop_update
	if inserted_sheep >= maximal_sheep_number:
		return stop_update
	last_spawn = time 

	var sheep = sheeps[inserted_sheep]
	inserted_sheep += 1

	var field_size = map.get_node("Field/Collision").shape.size 
	var field_position = map.get_node("Field").position 
	
	var rndX = rng.randi_range(0, field_size.x)
	var rndY = rng.randi_range(0, field_size.y)
	sheep.position = field_position + Vector2(rndX, rndY)
	
	sheep.set_is_born()
	
	return stop_update


## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_always_true(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return true

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_always_false(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return false

func count_dowm(_sm):
	return limit_time - (_sm.get_time() - _sm.get_last_activation_time("SPAWN"))

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_player_make_a_game_over(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return count_dowm(_sm) < 0

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_all_sheeps_were_captured(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return captured_sheep >= total_sheep

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_is_outside(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return not player_is_at_home

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_is_at_home(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return player_is_at_home

## _sm is the machine state
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func ef_dialog_is_finished(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return dialog.tag_has_been_raised("END")

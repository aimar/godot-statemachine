extends Area2D

var velocity : int = 200

func _ready():
	pass

func _process(delta):
	var x_sens = 0
	var y_sens = 0
	
	if Input.is_action_pressed("move_down"):
		y_sens = 1
	if Input.is_action_pressed("move_up"):
		y_sens = -1
	if Input.is_action_pressed("move_left"):
		x_sens = -1
	if Input.is_action_pressed("move_right"):
		x_sens = 1
	if x_sens != 0 or y_sens  != 0:
		rotation = atan2(x_sens, -y_sens)
	position += Vector2(x_sens, y_sens) * velocity * delta

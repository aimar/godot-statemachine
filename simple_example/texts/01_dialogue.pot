msgid ""
msgstr ""

msgid "Bienvenu dans ce petit jeu !"
msgstr ""

msgid "Ceci est votre personnage."
msgstr ""

msgid "Vous pouvez le déplacer avec les flèches directionnelles."
msgstr ""

msgid "Des moutons vont apparaître dans le champ."
msgstr ""

msgid "Le champ est ici !"
msgstr ""

msgid "Vous devez rentrer %s mouton(s) et vous rendre à la maison pour gagner."
msgstr ""

msgid "La Maison est ici !"
msgstr ""

msgid "Pour cela, vous devez rabattre les moutons dans l'enclos"
msgstr ""

msgid "L'enclos est ici."
msgstr ""

msgid "Si vous n'avez pas rabattu les moutons en moins de %s seconde(s), vous perdez !"
msgstr ""

msgid "A vous de jouer !"
msgstr ""


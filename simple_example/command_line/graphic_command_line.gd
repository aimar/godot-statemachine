class_name GraphicalCommandLine
extends VBoxContainer

var print_to_terminal : bool
var command_line : CommandLine
var line_edit : LineEdit
var text_edit : TextEdit
var line : int

func command_clear(st: CommandLine.CommandStream, args: Array[String]) -> bool :
	var error = false
	if len(args) != 0:
		st.print_err("Usage : clear")
		return error
	text_edit.clear()
	line = 0
	return error

func _init(_command_line : CommandLine=null):
	name = StringName("game_command_line")
	print_to_terminal = true
	line = 0
	if _command_line == null:
		command_line = GameProvider.command_line
	else:
		command_line = _command_line
	command_line.register_command(
		"clear", Callable(command_clear), "Clear the terminal"
	)
	line_edit = LineEdit.new()
	line_edit.name = StringName("cl_prompt")
	line_edit.size_flags_horizontal = Control.SIZE_FILL
	text_edit = TextEdit.new()
	text_edit.name = StringName("cl_text")
	text_edit.size_flags_horizontal = Control.SIZE_FILL
	text_edit.size_flags_vertical = Control.SIZE_EXPAND_FILL
	line_edit.connect("text_submitted", Callable(self, "line_edit_submit_a_text"))
	add_child(line_edit)
	add_child(text_edit)
	command_line.print_message = Callable(self, "print_message")
	command_line.print_error = Callable(self, "print_error")
	print_message("Type help to obtain some help.")
	
func redirect_to_terminal(val:bool):
	print_to_terminal = val

func print_message(text:String):
	text_edit.insert_line_at(line, text)
	if print_to_terminal:
		print(text)
	line += 1

func print_error(text:String):
	text_edit.insert_line_at(line, text)
	if print_to_terminal:
		print(text)
	line += 1

func line_edit_submit_a_text(new_text: String):
	if print_to_terminal:
		print(new_text)
	print_message("# %s"%new_text)
	command_line.exec_command(new_text)
	print_message("")
	line_edit.clear()

func dispose():
	command_line = null
	line_edit = null
	text_edit = null


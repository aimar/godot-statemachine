class_name UnitTestCommandLine
extends GdUnitTestSuite

var command_line : CommandLine
var test_directory = "user://tests/command_line"
var test_path = test_directory + "/history.txt"

var res = []

var print_msg = func(text:String):
	res.append(text)

var print_err = func(text:String):
	res.append(text)

func before():
	DirAccess.make_dir_recursive_absolute(test_directory)
	var error = DirAccess.copy_absolute(
		"res://command_line/test_history.txt", test_path
	)
	assert_bool(error == OK).is_true()
	assert_file(test_path).exists()

func test_help():
	command_line = CommandLine.new(test_path)
	command_line.print_message = Callable(print_msg)
	command_line.print_error = Callable(print_err)

	res = []
	command_line.exec_command("help")
	assert_array(res).is_equal(
		["Avalaible commands : ", " - help : Print the help to use the command line"]
	)

	res = []
	command_line.exec_command("help a")
	assert_array(res).is_equal(
		["The command a is Unknown."]
	)

	res = []
	command_line.exec_command("help help")
	assert_array(res).is_equal(
		["  Print the help to use the command line"]
	)

func test_get_history():
	command_line = CommandLine.new(test_path)
	command_line.print_message = Callable(print_msg)
	command_line.print_error = Callable(print_err)
	assert_str(str(command_line.get_history())).is_equal(
		'["command1 0 1 2 3", "command2", "command3 0"]'
	)
	res = []
	command_line.exec_command("help")
	command_line.exec_command("help a")
	command_line.exec_command("INVALID_COMMAND")
	command_line.exec_command("help help")
	assert_str(str(command_line.get_history())).is_equal(
		'["command1 0 1 2 3", "command2", "command3 0", "help", "help a", "help help"]'
	)

func files_are_identic(path_1, path_2) -> bool:
	var file_1 = FileAccess.open(path_1, FileAccess.READ)
	assert_bool(file_1 != null).is_true()
	var content_1 = file_1.get_as_text()
	var file_2 = FileAccess.open(path_2, FileAccess.READ)
	assert_bool(file_2 != null).is_true()
	var content_2 = file_2.get_as_text()
	return content_1 == content_2

func test_save_history():
	command_line = CommandLine.new(test_path)
	DirAccess.remove_absolute(test_directory + "/saved_history.txt")
	command_line.save_history(test_directory + "/saved_history.txt")
	assert_bool(
		files_are_identic(test_path, test_directory + "/saved_history.txt")
	).is_true()

func after():
	pass

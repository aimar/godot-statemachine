class_name CommandLine
extends RefCounted

var commands : Dictionary
var stream : CommandStream
var print_message : Callable
var print_error : Callable
var history : Array[String]

class Command:
	extends RefCounted
	var name : String = ""
	var call_back : Callable
	var description : String = ""
	var help : String = ""

class CommandStream:
	extends RefCounted
	var cmd_line : CommandLine
	func _init(cmd_l:CommandLine):
		cmd_line = cmd_l
	func print_msg(text:String):
		cmd_line.print_message.call(text)
	func print_err(text:String):
		cmd_line.print_error.call(text)

func command_help(st: CommandStream, args: Array[String]) -> bool :
	if len(args) == 0:
		st.print_msg("Avalaible commands : ")
		for command_name in commands:
			st.print_msg( " - %s : %s"%[command_name, commands[command_name].description])
	else:
		var command_name = args[0]
		if not command_name in commands :
			st.print_msg("The command %s is Unknown."%[command_name])
		else:
			var cmd = commands.get(command_name)
			st.print_msg("  " + cmd.description)
			if cmd.help != "":
				st.print_msg("")
				st.print_msg(cmd.help)
	return false

func auto_complete_command(_text):
	return ""

func print_stdout(text):
	print(text)

var regex
var regex_line

var MAX_HISTROY_SIZE :int = 200
var history_size :int = 200
var history_start :int = 0
var history_end :int = 0
var history_path : String

func append_to_history(text:String):
	history[history_end] = text
	history_end = (history_end + 1) % history_size
	if history_start == history_end :
		history_start = (history_start + 1) % history_size

func get_history() -> Array[String]:
	var res : Array[String] = []
	var cpt : int = history_start
	while cpt != history_end:
		res.append(history[cpt])
		cpt = (cpt + 1)%history_size
	return res

func _init(
	hist_path:String = "user://command_line/history.txt",
	hist_size:int = MAX_HISTROY_SIZE
):
	history_size = hist_size
	history_path = hist_path
	regex = RegEx.new()
	regex.compile("\\S+")
	regex_line = RegEx.new()
	regex_line.compile("[^\\n]+")

	history = []
	history.resize(history_size)
	history_start = 0
	history_end = 0
	if FileAccess.file_exists(history_path):
		var hist_file = FileAccess.open(history_path, FileAccess.READ)
		if hist_file == null:
			print("Impossible to open %s."%history_path)
		else:
			var hist_text : String = hist_file.get_as_text()
			hist_file.close()
			for result in regex_line.search_all(hist_text):
				append_to_history(result.get_string())

	print_message = Callable(print_stdout)
	print_error = Callable(print_stdout)
	stream = CommandStream.new(self)
	commands = {}
	register_command("help", Callable(command_help), "Print the help to use the command line")

func save_history(hist_path:String = ""):
	if hist_path == "":
		hist_path = history_path
	var res : String = ""
	for cmd in get_history():
		res += cmd + "\n"
	var hist_file = FileAccess.open(hist_path, FileAccess.WRITE)
	if hist_file == null :
		print("Impossible to open %s."%history_path)
	else:
		hist_file.store_string(res)
		hist_file.close()

func register_command(
	name:String, call_back:Callable,
	description:String="", help:String="" 
):
	var command = Command.new()
	command.help = help
	command.description = description
	command.call_back = call_back
	assert(not name in commands)
	commands[name] = command

func get_commands(text:String) -> Array[String]:
	var res : Array[String] = []
	for cmd in history:
		if cmd.begins_with(text):
			res.append(text)
	for cmd in commands:
		if cmd.begins_with(text):
			res.append(text)
	return res

func exec_command(text:String):
	var command_array : Array[String] = []
	for result in regex.search_all(text):
		command_array.push_back(result.get_string())
	if len(command_array) == 0:
		return
	var command_name : String = command_array[0]
	if not command_name in commands:
		stream.print_err("The command %s don't exists."%command_name)
		return
	var call_back : Callable = commands.get(command_name).call_back
	var res = call_back.call(stream, command_array.slice(1))
	if res :
		stream.print_err("Command error : %s"%[res])
	else:
		append_to_history(text)

func dispose():
	commands = {}
	stream = null
	print_message = Callable()
	print_error = Callable()
	history = []

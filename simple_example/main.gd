extends Node

var state_machine : SMStateMachine
var state_machine_uid : String
var hide_debug_flag : bool
var hide_menu_flag : bool

func command_quit(st: CommandLine.CommandStream, args: Array[String]) -> bool :
	var error = false
	if len(args) != 0:
		st.print_err("Usage : quit")
		return error
	st.print_msg("We quit the game. Bye !")
	GameProvider.quit_game()
	return error

func init_scenario():
	state_machine = SMStateMachine.new()
	state_machine.load_json("res://state_machines/scenario_main.json")
	var state_machine_api = preload("res://state_machines/scenario_main_api_impl.gd").new(self)
	state_machine.register_api(state_machine_api)

func start_scenrio():
	state_machine_uid = GameProvider.declare_state_machine_to_debuger(
		state_machine,
		preload("res://state_machines/scenario_main.tscn")
	)
	state_machine.start()
	
func close_scenario():
	if state_machine != null:
		GameProvider.remove_state_machine_from_debuger(state_machine_uid)
		state_machine.dispose(true)
		state_machine = null

func update_scenario(_delta):
	if state_machine != null:
		state_machine.update()

func _init():
	state_machine = null
	hide_debug_flag = true
	hide_menu_flag = false
	GameProvider.command_line.register_command(
		"quit", Callable(command_quit), "Quit the game"
	)

func _ready():
	if hide_debug_flag:
		hide_debug()
	else:
		hide_debug()

	if hide_menu_flag:
		hide_menu()
	else:
		show_menu()

func _process(_delta:float):
	update_scenario(_delta)
	
	if Input.is_action_just_pressed("show_debugger"):
		swap_debug()
	
	if Input.is_action_just_pressed("show_main_menu"):
		swap_menu()

func hide_menu():
	hide_menu_flag = true
	get_node("Menu").hide()
	
func show_menu():
	hide_menu_flag = false
	get_node("Menu").show()

func swap_menu():
	if hide_menu_flag:
		hide_debug()
		show_menu()
	else:
		hide_menu()

func hide_debug():
	hide_debug_flag = true
	get_node("Debug").hide()
	
func show_debug():
	hide_debug_flag = false
	get_node("Debug").show()

func swap_debug():
	if hide_debug_flag:
		show_debug()
	else:
		hide_debug()

func _on_quit_game_pressed():
	GameProvider.quit_game()

func _on_start_game_pressed():
	hide_menu()
	close_scenario()
	init_scenario()
	start_scenrio()

func _on_return_to_game_pressed():
	hide_menu()

func _on_show_console_pressed():
	hide_menu()
	show_debug()

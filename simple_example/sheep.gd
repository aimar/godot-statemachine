extends Area2D

var state_machine = SMStateMachine
var state_machine_api
var state_machine_uid

func init(map):
	state_machine = SMStateMachine.new()
	state_machine.load_json("res://state_machines/sheep.json")
	state_machine_api = preload("res://state_machines/sheep_api_impl.gd").new(map, self)
	state_machine.register_api(state_machine_api)

func set_is_born():
	state_machine_api.set_is_born()

func _enter_tree():
	state_machine_uid = GameProvider.declare_state_machine_to_debuger(
		state_machine,
		preload("res://state_machines/sheep.tscn")
	)
	state_machine.start()

func _exit_tree():
	GameProvider.remove_state_machine_from_debuger(state_machine_uid)
	state_machine.dispose(true)

func _process(_delta):
	state_machine.update()

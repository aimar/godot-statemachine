@tool
extends Control

var last_scene = null
var plugin_editor : EditorPlugin

@export var viewer_path : String = "res://tools/object_viewer/object_viewer.tscn"
@export var game_path : String = "res://game_main_launcher.tscn"

var tool_scenes = [viewer_path, game_path]
var current_scene = null

enum GameToolAction {
	NONE,
	GENERATE_TRAD_API
}

var action : GameToolAction

func register_plugin_editor(_plugin_editor):
	plugin_editor = _plugin_editor

func _init():
	action = GameToolAction.NONE

func _enter_tree():
	pass

func _process(delta):
	pass

func path_is_a_json_existing_file(path:String):
	var text : String
	if not path.ends_with(".json"):
		text = "Select a json file."
		print(text)
		push_error(text)
		return false
	if not FileAccess.file_exists(path):
		text = "File don't exists !"
		print(text)
		push_error(text)
		return false
	return true

func generate_trad_files(paths:PackedStringArray):
	for path in paths:
		generate_trad_file(path)

func generate_trad_file(path:String):
	var text : String = ""
	print("Generating traduction files (.po and .pot) for : " + str(path))
	if not path_is_a_json_existing_file(path):
		return
	var dialog = preload("res://core/linear_dialog.gd").new(path)
	var pot_path : String = path.get_basename() + ".pot"
	var res = dialog.generate_pot(pot_path)
	if res :
		print("Generation of : " + str(pot_path) + " is a success.")
	else:
		text = "Failed to generate : " + pot_path
		printerr(text)
		push_error(text)
		return false
	var locales = {
		"fr" : "fr_FR.utf8",
		"en" : "en_GB.utf8",
		# "es" : "es_ES.utf8",
	}
	for lang in locales:
		var po_path : String = path.get_basename() + "_" + lang + ".po"
		res = dialog.generate_po_file_on_linux(
			pot_path, po_path, locales[lang]
		)
		if res :
			print("Generation of : " + str(po_path) + " is a success.")
			plugin_editor.get_editor_interface().get_resource_filesystem().update_file(po_path)
		else:
			text = "Failed to generate : " + po_path
			printerr(text)
			push_error(text)
			return false
	return true

func _on_trad_gen_pressed():
	action = GameToolAction.GENERATE_TRAD_API
	$FileDialog.show()

func _on_file_dialog_files_selected(paths):
	$FileDialog.hide()
	if action == GameToolAction.GENERATE_TRAD_API:
		generate_trad_files(paths)
	else:
		var text = "Unknown action %s"%action
		printerr(text)
		push_error(text)
	action = GameToolAction.NONE

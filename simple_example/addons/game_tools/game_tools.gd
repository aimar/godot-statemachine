@tool
extends EditorPlugin

var dock

func _enter_tree():
	print("user:// directory is present here : %s "% OS.get_user_data_dir() )
	dock = preload("res://addons/game_tools/game_tools_dock.tscn").instantiate()
	dock.register_plugin_editor(self)
	# add_control_to_dock( DOCK_SLOT_RIGHT_BL, )
	add_control_to_dock(EditorPlugin.DOCK_SLOT_LEFT_UR, dock)
	
func _exit_tree():
	remove_control_from_docks(dock)
	dock.free()

extends Node

var command_line:CommandLine

func _init():
	id = 0
	command_line = CommandLine.new()

func _enter_tree():
	pass

func _exit_tree() :
	pass

func _ready():
	pass

func _process(_delta:float):
	for ms_viewer in machine_states_viewers:
		var machine_state : SMStateMachine = machine_states_viewers[ms_viewer][0]
		var machine_state_viewer : StateMachineEditor = machine_states_viewers[ms_viewer][1]
		machine_state_viewer.show_active_states(
			machine_state.actives_states_to_uuid_states()
		)

func get_scenario_root():
	return get_tree().get_root().get_node("Main/Scenario")

func get_menu_root():
	return get_tree().get_root().get_node("Main/Menu")

var id : int
var machine_states_viewers : Dictionary

func declare_state_machine_to_debuger(
	state_machine : SMStateMachine,
	state_machine_scene : PackedScene
) -> String :
	var state_machine_viewer=StateMachineEditor.new()
	state_machine_viewer.show_zoom_label=false
	state_machine_viewer.show_edit_button=false
	state_machine_viewer.free_sme_spec_data_after_drawing=true
	var force_redraw = true
	var sm_name : String = state_machine.sm_name + "_%s"%id
	id += 1
	state_machine_viewer.set_data(
		state_machine_scene,
		state_machine_scene.instantiate(),
		state_machine,
		sm_name,
		force_redraw
	)
	state_machine_viewer.size_flags_horizontal += Control.SIZE_EXPAND
	state_machine_viewer.size_flags_vertical += Control.SIZE_EXPAND
	machine_states_viewers[sm_name] = [state_machine, state_machine_viewer]
	get_tree().get_root().get_node("Main/Debug/StateMachines").add_child(
		state_machine_viewer
	)
	return sm_name

func remove_state_machine_from_debuger(machine_state_id: String):
	if not machine_state_id in machine_states_viewers:
		var text = "Unknown machine state : %s"%machine_state_id
		printerr(text)
		push_error(text)
		return
	get_tree().get_root().get_node("Main/Debug/StateMachines").remove_child(
		machine_states_viewers[machine_state_id][1]
	)
	machine_states_viewers[machine_state_id][1].queue_free()
	machine_states_viewers.erase(machine_state_id)

func quit_game():
	get_tree().quit()

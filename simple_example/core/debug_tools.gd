class_name DBG

static func print_the_stack() -> bool :
	return false

static func accept_all() -> bool :
	return true

static func white_list() -> Array[String] :
	return [
		#"dispose",
		#"pause",
		#"trace"
	]
static func black_list() -> Array[String] :
	return [
		"dispose",
		"pause",
		"trace"
	]

## Print the object in parameter with the line and source code.
##
## 'obj' can be empty or contain any object
## 'types' should be a string or an array of String.
##         The print function will print 'obj' if the type is not ine the 
##         black_list and if (accept_all variable is set to true or the type 
##         is in the white_list).
@warning_ignore("shadowed_global_identifier")
static func print(obj : Variant="", types : Variant="", _print_stack:bool=false):
	var _white_list = DBG.white_list()
	var _black_list = DBG.black_list()
	if typeof(types) != TYPE_ARRAY:
		types = [types]
	var in_white_list:bool = DBG.accept_all()
	for type in types:
		if type in _black_list:
			return
		in_white_list = in_white_list or (type in _white_list)
	if not in_white_list:
		return
	var text : String = str(obj)
	var frame = get_stack()
	if len(frame) < 2:
		print(text + " - line ? : no debug server or in thread mode (see get_stack())")
		return
	var line = "line: " + str(frame[1]["line"])
	var source = str(frame[1]["source"]) #.get_file())
	print("%s - %s, %s"% [text, line, source])
	if(DBG.print_the_stack() or _print_stack):
		print("frame : ")
		for tt in frame:
			print("    " + str(tt))

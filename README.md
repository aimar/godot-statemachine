# Installation

To install : 
'''
git submodule init
git submodule update
'''

Download the last version of the Godot Engine, present in the following website : 
'''
https://godotengine.org/download/archive/
'''

# To launch the SimpleExample

Launch the godot engine and import the project present here : 
'''
example/project.godot
'''

# Copyright and Licence for the SimpleExample

Copyright (C) 2023 Jean_La_Tomate.
Copyright (C) 2023 Adrien Boussicault.

This program is free software: you can redistribute it and/or modify it under the terms of th  e GNU General Public License as published by the Free Software Foundation, either version 3 o  f the License, or (at your option) any later version.
                                                                                   
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; wit  hout even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See th  e GNU General Public License for more details.
                                                                                   
You should have received a copy of the GNU General Public License along with this program. If   not, see <https://www.gnu.org/licenses/>.

